
--[[
    =======================================================
    Logging 
    =======================================================
  ]]

-- Set to false to turn off all logs.
enable_logging = false;

function log(msg)
    if enable_logging then
        reaper.ShowConsoleMsg(msg);
    end
end

--[[
    =======================================================
    A tiny IMGUI framework. 
    =======================================================
  ]]

-- global gui state
--   event type: 0: repaint, 1: mouse-down, 2: mouse-up, 3: key-down
--   key_code: the key code for the last key-down event
gui_state = { ['event_type'] = 0, ['key_code'] = 0, ['mouse_down'] = false, ['skin'] = { ['fgcolor'] = {1,1,1,1}, ['bgcolor'] = {0,0,0,1} } }

-- Input: A point (x,y) and a rect ({x,y,w,h}). 
-- Return: A boolean indicating whether the point locates inside the rect.
function is_in_rect(x, y, rect)
    if (x < rect[1]) or (x > (rect[1] + rect[3])) then
        return false;
    elseif (y < rect[2]) or (y > (rect[2] + rect[4])) then
        return false;
    end
    return true;
end

-- Input: A color (An array of at least 3 number elements. Each element ranges from 0 to 1.)
function set_color(c)
    gfx.r = tonumber(c[1]);
    gfx.g = tonumber(c[2]);
    gfx.b = tonumber(c[3]);
    if c[4] == nil then
        gfx.a = 1;
    else
        gfx.a = tonumber(c[4]);
    end
end

function set_color_fg()
    set_color(gui_state['skin']['fgcolor']);
end

function set_color_bg()
    set_color(gui_state['skin']['bgcolor']);
end

-- [IMGUI Widget] Button
-- Input: A rect ({x,y,w,h}) for the widget. A label (string) show on the button.
-- Return: A boolean value -- 'true' when clicked, 'false' otherwise.
function button(rect, label)

    if gui_state.event_type == 0 then  -- repaint

        local offset = 0;
        set_color_fg();

        -- Check if the mouse cursor locates inside the rect and the mouse button is clicking-down.
        -- Apply an offset on the rect to simulate the 'pressed-down' visual effect.
        if (gfx.mouse_cap & 1 == 1) and is_in_rect(gfx.mouse_x, gfx.mouse_y, rect) then
            offset = 3;
        end

        -- Draw button outline
        gfx.roundrect(rect[1] + offset, rect[2] + offset, rect[3], rect[4], 8);

        -- Draw button label (centered)
        local labelstr = tostring(label);
        local w, h = gfx.measurestr(labelstr);
        gfx.x = rect[1] + offset + ((rect[3] - w) // 2);
        gfx.y = rect[2] + offset + ((rect[4] - h) // 2);
        gfx.drawstr(labelstr);

    elseif gui_state.event_type == 2 then  -- mouse-up
        if is_in_rect(gfx.mouse_x, gfx.mouse_y, rect) then
            return true;
        end
    end

    return false;
end

-- [IMGUI Widget] Label
-- Input: A rect ({x,y,w,h}) for the widget. A label (string) to show.
-- Return: No return value.
function label(rect, label)
    if gui_state.event_type == 0 then -- repaint
        -- draw button label (v-centered)
        set_color_fg();
        local labelstr = tostring(label);
        local w, h = gfx.measurestr(labelstr);
        gfx.x = rect[1];
        gfx.y = rect[2] + ((rect[4] - h) // 2);
        gfx.drawstr(labelstr);
    end
end

-- [IMGUI Widget] Text Field (single line)
-- Input: A rect ({x,y,w,h}) for the widget. A state object (table, should be an empty table ({}) on the first call).
-- Return: Updated object ({text,cursoridx,cursordrew}).
function textfield(rect, state)
    if gui_state.event_type == 0 then -- repiant
        -- backgroud fill
        set_color_fg();
        gfx.rect(rect[1], rect[2], rect[3], rect[4]);

        -- draw text (v-centered)
        set_color_bg();
        if state.text ~= nil then
            local t = tostring(state.text);
            local w, h = gfx.measurestr(t);
            gfx.x = rect[1] + 2;
            gfx.y = rect[2] + ((rect[4] - h) // 2) + 2;
            gfx.drawstr(t);
        end

        -- draw cursor when needed
        if state.cursoridx ~= nil then
            -- Note: Only draw cursor on every other frame. To simulate a 'flashing' visual effect.
            if (state.cursordrew == nil) or (state.cursordrew == false) then
                state.cursordrew = true; -- skip drawing
            else
                state.cursordrew = false;
                local cidx = tonumber(state.cursoridx);

                if state.text ~= nil then
                    local t = tostring(state.text:sub(1, cidx));
                    local w, h = gfx.measurestr(t);
                    gfx.x = rect[1] + w + 2;
                    gfx.y = rect[2] + 2;
                    gfx.lineto(rect[1] + w + 2, rect[2] + rect[4] - 2);
                else
                    gfx.x = rect[1] + 2;
                    gfx.y = rect[2] + 2;
                    gfx.lineto(rect[1] + 2, rect[2] + rect[4] - 2);
                end
            end
        end
    elseif gui_state.event_type == 2 then
        -- mouse-up
        if is_in_rect(gfx.mouse_x, gfx.mouse_y, rect) and (state.cursoridx == nil) then
            if state.text == nil then
                state.cursoridx = 1
            else
                state.cursoridx = #state.text + 1;
            end
        end
    elseif gui_state.event_type == 3 then
        -- key-up
        if state.cursoridx ~= nil then
            local ch = gui_state.key_code;
            if (ch > 31) and (ch < 127) then
                state.cursoridx = state.cursoridx + 1;

                if state.text == nil then
                    state.text = string.char(ch);
                else
                    state.text = state.text .. string.char(ch);
                end
            elseif ch == 8 then  -- backspace
                if state.cursoridx > 1 then
                    state.cursoridx = state.cursoridx - 1;
                    state.text = state.text:sub(1, state.cursoridx - 1);
                else
                    state.cursoridx = 1;
                end
            elseif ch == 13 then -- enter
                state.cursoridx = nil;
            end
        end
    end

    return state;
end

-- [IMGUI Widget] Checkbox
-- Input: A rect ({x,y,w,h}) for the widget. A label string (optional). Current checked state.
-- Return: A boolean indicating the updated checked state.
function checkbox(rect, text, checked)
    if gui_state.event_type == 0 then -- repaint
        set_color_fg();
        local off = (rect[4] - 14) // 2;
        gfx.rect(rect[1] + off, rect[2] + off, 14, 14, false); -- outline of checkbox

        -- draw check
        if checked then
            local w, h = gfx.measurestr('v');
            gfx.x = rect[1] + off + ((14 - w)//2);
            gfx.y = rect[2] + off + ((14 - h)//2);
            gfx.drawstr('v');
        end

        -- draw text
        if text ~= nil then
            local t = tostring(text);
            local w2,h2 = gfx.measurestr(t);
            gfx.x = rect[1] + (off * 2) + 14;
            gfx.y = rect[2] + ((rect[4] - h2)//2);
            gfx.drawstr(t);
        end
    elseif gui_state.event_type == 2 then -- mouse-up
        if is_in_rect(gfx.mouse_x, gfx.mouse_y, rect) then
            checked = not checked;
        end
    end
    return checked;
end

--[[
    =======================================================
    Helper functions for persistent properties. 
    =======================================================
  ]]
function getprop(key)
    local retval, str = reaper.GetProjExtState(0, 'chorddonkey', key);
    -- retval is 1 when the key exist. otherwise, 0.
    -- log('retval: ' .. tostring(retval) .. ', str: ' .. tostring(str) .. '\n');
    if retval == 0 then
        return nil;
    end
    return str;
end

function setprop(key, val)
    reaper.SetProjExtState(0, 'chorddonkey', key, val);
end

function delprop(key)
    reaper.SetProjExtState(0, 'chorddonkey', key, "");
end

function delallprops(key)
    reaper.SetProjExtState(0, 'chorddonkey', "", "");
end

--[[
    =======================================================
    Chord notation parser
    =======================================================
  ]]
 
-- Input: midi_num: An integer for the number of MIDI note.
-- Return: A string for the note notation of that MIDI note.
--         Or nil if midi_num is not an number.
function get_note_name(midi_num)
    if "number" ~= type(midi_num) then
        log('Error: get_note_name() expecting a number input.');
        return nil;
    end

    -- Assume 60 is the middle C (C4), compute the octave offset.
    local octave_offset = math.floor((midi_num - 60) // 12);
    -- Normalize to be in the range of 0~11
    local normalized_num = math.floor(midi_num - 60 - (octave_offset * 12));

    if note_table == nil then
        note_table = { 'C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'A#', 'B' };
    end

    -- Compose the result
    return note_table[normalized_num+1] .. tostring(4 + octave_offset);
end

-- Input: note_name: A string for the note notation.
-- Return: A number of MIDI note for the input.
--         Or nil if note_name is either not an string, or in ill-form.
function get_midi_num(note_name)
    -- NOTE: Barely no protection! Assume note_name is already in lowercase.
    if 'string' ~= type(note_name) then
        return nil;
    end

    if midi_table == nil then
        midi_table = { ['c'] = 60, ['d'] = 62, ['e'] = 64, ['f'] = 65, 
                       ['g'] = 67, ['a'] = 69, ['b'] = 71 };
    end
    
    -- Match the tonic part
    local tonic_result = note_name:match('^[a-g][#b]?');
    if nil == tonic_result then
        log('Error: get_midi_num() Unrecognized tonic note from: ' .. note_name);
        return nil;
    end
    local num = midi_table[tonic_result:sub(1,1)];

    -- Try match the sharp or flat symbol whenever possible
    if 2 == #tonic_result then
        if tonic_result:byte(2) == 35 then  -- '#' : 35
            num = num + 1;
        else
            num = num - 1;
        end
    end
    
    -- Try match the octave number whenever possible
    if #tonic_result < #note_name then
        local octave_part = note_name:sub(#tonic_result + 1);
        local octave_result = octave_part:match('^%d+');
        if nil ~= octave_result then
            num = num + ((tonumber(octave_result) - 4) * 12);
        end
    end

    return num;
end

--[[ 
     Take the chord type notation as input, try to figure out 
     the end note (N-th from the tonic) of the chord and return.
  ]]
 function find_chord_end(notation)
    if 'string' ~= type(notation) then
        return 0;
    end
    local r = notation:match('%d+$');
    if nil == r then
        return 5;
    end
    r = tonumber(r);
    r = ((r // 2) * 2) + 1; -- ensure to be an odd number
    return math.min(math.max(r, 5), 13);
end

-- Input: nstr is a string of a chord notation. Ex: 'Cmaj9sus', 'E7', 'Gdim', 'Dm7(#5)' ...
-- Return: A array of MIDI notes (numbers) for the input chord notation.
--         The tonic is always in the range of C4(60) ~ C5(72).
--         Or return nil on error (ill-formed chord notation, etc.)
function parse_chord(nstr)
    -- 'nstr' should be a string
    if 'string' ~= type(nstr) then
        log('Error: parse_chord() expects a string input.\n');
        return nil;
    end

    -- to lower case
    local instr = nstr:lower();

    -- step1. Match and process the tonic (root) note from the head of 'instr'
    local tonic_result = instr:match('^[a-g][#b]?');
    if nil == tonic_result then
        reaper.ShowMessageBox('Chord notation (' .. nstr .. ') does not start with a valid tonic note.', 'Error', 0);
        return nil;
    end

    local note_nums = {0,2,4,5,7,9,11,12,14,16,17,19,21}; -- major scale (offset to the 1st note)
    local note_picked = {true,false,false,false,false,false,false,false,false,false,false,false,false};

    note_nums[1] = get_midi_num(tonic_result);

    -- step2. Match and process the inversion expression (if any) from the tail of 'instr'
    local inversion_result = instr:match('/[a-g][#b]?$');

    local middle_start = #tonic_result + 1;
    local middle_end = -1;
    if nil ~= inversion_result then
        middle_end = middle_end - #inversion_result;
    end
    local middle_part = instr:sub(middle_start, middle_end);
    local end_note = 5;

    -- step3. Match and process the main chord type from the head of the remaining part of 'instr'.
    local type_result = middle_part:match('^ion');
    if nil ~= type_result then -- Ionian 2,2,1,2,2,2,1
        local off = note_nums[1] - 60;
        return {off+60, off+62, off+64, off+65, off+67, off+69, off+71};
    end

    type_result = middle_part:match('^dor');
    if nil ~= type_result then -- Dorian 2,1,2,2,2,1,2
        local off = note_nums[1] - 60;
        return {off+60, off+62, off+63, off+65, off+67, off+69, off+70};
    end

    type_result = middle_part:match('^phr');
    if nil ~= type_result then -- Phrygian 1,2,2,2,1,2,2
        local off = note_nums[1] - 60;
        return {off+60, off+61, off+63, off+65, off+67, off+68, off+70};
    end

    type_result = middle_part:match('^lyd');
    if nil ~= type_result then -- Lydian 2,2,2,1,2,2,1
        local off = note_nums[1] - 60;
        return {off+60, off+62, off+64, off+66, off+67, off+68, off+70};
    end

    type_result = middle_part:match('^mix');
    if nil ~= type_result then -- Mixolydian 2,2,1,2,2,1,2
        local off = note_nums[1] - 60;
        return {off+60, off+62, off+64, off+65, off+67, off+69, off+70};
    end

    type_result = middle_part:match('^aeo');
    if nil ~= type_result then -- Aeolian 2,1,2,2,1,2,2
        local off = note_nums[1] - 60;
        return {off+60, off+62, off+63, off+65, off+67, off+68, off+70};
    end

    type_result = middle_part:match('^loc');
    if nil ~= type_result then -- Locrian 1,2,2,1,2,2,2
        local off = note_nums[1] - 60;
        return {off+60, off+61, off+63, off+65, off+66, off+68, off+70};
    end

    type_result = middle_part:match('^blue');
    if nil ~= type_result then -- Blue
        local off = note_nums[1] - 60;
        return {off+60, off+63, off+65, off+66, off+67, off+70};
    end

    type_result = middle_part:match('^china');
    if nil ~= type_result then -- china
        local off = note_nums[1] - 60;
        return {off+60, off+62, off+64, off+67, off+69};
    end

    type_result = middle_part:match('^japan');
    if nil ~= type_result then -- japan
        local off = note_nums[1] - 60;
        return {off+60, off+61, off+65, off+67, off+70};
    end

    type_result = middle_part:match('^japandown');
    if nil ~= type_result then -- japan-down
        local off = note_nums[1] - 60;
        return {off+60, off+61, off+65, off+67, off+68};
    end

    type_result = middle_part:match('^flam');
    if nil ~= type_result then -- Flamenco: Phrygian #3
        local off = note_nums[1] - 60;
        return {off+60, off+61, off+64, off+65, off+67, off+68, off+70};
    end

    type_result = middle_part:match('^pow');
    if nil ~= type_result then
        -- handle power chord (the 1st and the 5th note)
        note_nums[5] = note_nums[1] + note_nums[5];
        note_picked[5] = true;
        goto done_chord_type;
    end

    type_result = middle_part:match('^hdim[7]?');
    if nil ~= type_result then
        -- handle hdim chord (half-diminished 7)
        note_picked[3] = true; note_nums[3] = note_nums[1] + note_nums[3] - 1;
        note_picked[5] = true; note_nums[5] = note_nums[1] + note_nums[5] - 1;
        note_picked[7] = true; note_nums[7] = note_nums[1] + note_nums[7] - 1;
        goto done_chord_type;
    end

    type_result = middle_part:match('^dim[7]?');
    if nil ~= type_result then
        -- handle dim chord (diminished triad or 7)
        note_nums[3] = note_nums[1] + 3;
        note_nums[5] = note_nums[1] + 6;
        note_nums[7] = note_nums[1] + 9;
        note_picked[3] = true;
        note_picked[5] = true;
        if #type_result > 3 then
            note_picked[7] = true;
        end
        goto done_chord_type;
    end

    -- Populate note_nums.
    for idx = 2, 13 do
        note_nums[idx] = note_nums[idx] + note_nums[1];
    end

    -- handle major chord
    type_result = middle_part:match('^maj%d*');
    if nil ~= type_result then
        end_note = find_chord_end(type_result:sub(4));
        goto pick_notes;
    end

    -- handle minor chord (just like major chord but flat both 3th and 7th notes)
    type_result = middle_part:match('^m%d*');
    if nil ~= type_result then
        end_note = find_chord_end(type_result:sub(2));
        note_nums[3] = note_nums[3] - 1;
        note_nums[7] = note_nums[7] - 1;
        goto pick_notes;
    end

    -- handle dominant chord (like major chord but flat the 7th note)
    type_result = middle_part:match('^%d+');
    if nil ~= type_result then
        end_note = find_chord_end(type_result);
        note_nums[7] = note_nums[7] - 1;
        goto pick_notes;
    end

    -- If reach here, it means no explicit chord type has been specified. Treat it as "maj".

    ::pick_notes::
    for idx = 3,13,2 do
        if idx > end_note then
            break;
        end
        note_picked[idx] = true;
    end

    -- step4. Match and process the decorating expression from the remaining part of 'instr'.
    ::done_chord_type::
    local deco_part = middle_part;
    if nil ~= type_result then
        deco_part = middle_part:sub(#type_result+1);
    end

    while true do
        if #deco_part == 0 then
            break;
        end

        -- add N
        local opt_result = deco_part:match('^add%d+');
        if nil ~= opt_result then
            local n = math.min(math.max(tonumber(opt_result:sub(4)), 1), 13);
            note_picked[n] = true;
            goto process_next_deco;
        end

        -- omit N
        opt_result = deco_part:match('^omit%d+');
        if nil ~= opt_result then
            local n = math.min(math.max(tonumber(opt_result:sub(5)), 1), 13);
            note_picked[n] = false;
            goto process_next_deco;
        end

        -- aug N  (N=5 when absent)
        opt_result = deco_part:match('^aug%d*');
        if nil ~= opt_result then
            local n = 5;
            if #opt_result > 3 then
                n = math.min(math.max(tonumber(opt_result:sub(4)), 1), 13);
            end
            note_picked[n] = true;
            note_nums[n] = note_nums[n] + 1;
            goto process_next_deco;
        end

        -- sus N (N=4 when absent)
        opt_result = deco_part:match('^sus%d*');
        if nil ~= opt_result then
            local n = 4;
            if #opt_result > 3 then
                n = math.min(math.max(tonumber(opt_result:sub(4)), 1), 13);
            end
            note_picked[3] = false;
            note_picked[n] = true;
            goto process_next_deco;
        end

        -- 6/9  Remove the 7th note when possible, and then add both 6th and 9th notes.
        opt_result = deco_part:match('^[%s,]?6/9');
        if nil ~= opt_result then
            note_picked[6] = true;
            note_picked[7] = false;
            note_picked[9] = true;
            goto process_next_deco;
        end

        -- (#5,b9,...)  Quoted Nth note adjusting list.
        opt_result = deco_part:match('^%([a-g#,%d%s]*%)');
        if nil ~= opt_result then
            local opt_list = opt_result:sub(2,-2);  -- strip the '(' and ')' at both ends.
            while #opt_list > 0 do
                local opt = opt_list:match('^[#b]%d+[,]?');
                if opt == nil then
                    reaper.ShowMessageBox('Unrecognized quoted notation: ' .. tostring(opt_list), 'Error', 0);
                    return nil;
                end
                local offset = 1;
                if opt:byte(1) ~= 35 then -- '#' : 35
                    offset = -1;
                end
                local endidx = #opt;
                if opt:byte(endidx) == 44 then -- ',' : 44
                    endidx = endidx - 1;
                end
                local n = tonumber(opt:sub(2,endidx));
                n = math.min(math.max(tonumber(n), 1), 13);
                note_picked[n] = true;
                note_nums[n] = note_nums[n] + offset;
                opt_list = opt_list:sub(#opt + 1);
            end
            goto process_next_deco;
        end

        -- Unrecognized notation
        reaper.ShowMessageBox('Unrecognized notation: ' .. deco_part, 'Error', 0);
        do return nil; end

        ::process_next_deco::
        if nil ~= opt_result then
            deco_part = deco_part:sub(#opt_result + 1)
        end
    end

    local ret = {};

    -- Deal with the inversion. The inversion note should be placed at the head  
    -- of ret since they are always the lowest note.
    if inversion_result ~= nil then
        local n = get_midi_num(inversion_result:sub(2));
        n = n - 12;
        ret[1] = n;
    end

    -- Compose the final result
    for idx = 1,13 do
        if note_picked[idx] == true then
            ret[#ret + 1] = note_nums[idx];
        end
    end

    -- Print out the result and then return the result
    local text = '';
    for idx = 1,#ret do
        text = text .. get_note_name(ret[idx]) .. ', ';
    end
    log('Parse result: ' .. text .. '\n');

    return ret;
end

--[[
    =======================================================
    Main GUI loop and plugin logic
    =======================================================
  ]]

function persist_history()
    for i = 1,8 do
        local k = 'history' .. tostring(i);
        local v = chord_history[i];
        if v == nil then
            delprop(k);
        else
            setprop(k, chord_history[i]);
        end
    end
end

-- Handle the chord notation that user has inputted.
-- Input: A string for the chord notation.
-- Return: A boolean indicating whether the notes have been added.
--         When returning true, an undo state will be created automatically.
--         An the history should be updated as well.
function do_chord_notation(nstr)
    -- Parse the chord notation.
    local notes = parse_chord(nstr);
    if nil == notes then
        return false; -- on error
    end

    -- Try insert the notes to the active MIDI editor.
    local ppq_start = 0;
    local ppq_end = 0;
    local take = reaper.MIDIEditor_GetTake(active_editor);
    local mi = reaper.GetMediaItemTake_Item(take);
    if fulltake_check then
        if nil == mi then
            reaper.ShowMessageBox('Unable to get MediaItem info.', 'Error', 0);
            return false;
        end
        local start_sec = reaper.GetMediaItemInfo_Value(mi, 'D_POSITION'); -- in seconds.
        local len_sec = reaper.GetMediaItemInfo_Value(mi, 'D_LENGTH'); -- in seconds.
        ppq_start = reaper.MIDI_GetPPQPosFromProjTime(take, start_sec);
        ppq_end = reaper.MIDI_GetPPQPosFromProjTime(take, start_sec + len_sec);
    else
        ppq_start = reaper.MIDI_GetPPQPosFromProjTime(take, reaper.GetCursorPosition());
        local qn_sec = reaper.TimeMap_QNToTime_abs(0, reaper.MIDI_GetGrid(take));
        ppq_end = reaper.MIDI_GetPPQPosFromProjTime(take, reaper.GetCursorPosition() + qn_sec);
    end

    -- Insert notes
    local idx = 1;
    while notes[idx] ~= nil do
        reaper.MIDI_InsertNote(take, true, mute_check, ppq_start, ppq_end, 0, notes[idx], 100, true);
        idx = idx + 1;
    end
    reaper.MIDI_Sort(take);

    -- Undo: Explicitly mark current media item to Reaper's undo system.
    reaper.Undo_OnStateChange_Item(0, 'Notes added for chord (' .. nstr .. ')', mi);

    -- Manage history
    local idx = 1;
    local found = false;
    while true do
        if chord_history[idx] == nil then
            break;
        elseif chord_history[idx] == nstr then
            found = true;
            break;
        end
        idx = idx + 1;
    end

    local new_history = { nstr };
    for i = 1,8 do
        if chord_history[i] == nil then
            break;
        end

        if i ~= idx then
            new_history[#new_history + 1] = chord_history[i];
        end

        if #new_history >= 8 then
            break;
        end
    end
    chord_history = new_history;

    -- Persistent history items.
    persist_history();

    return true;
end

-- Main GUI layout function.
-- By the design of IMGUI, it also acts as an event responder.
function do_gui()
    label({10, 10, 200, 20}, 'Chord notation:');

    chordinput_state = textfield({30, 40, 200, 20}, chordinput_state);

    if button({270,40,100,20}, 'Insert') then
        chordinput_state.cursoridx = nil;
        if (chordinput_state.text ~= nil) and (#chordinput_state.text > 0) then
            if true == do_chord_notation(chordinput_state.text) then
                chordinput_state.text = nil;
            end
        end
    end

    local was_checked = mute_check;
    mute_check = checkbox({30, 70, 200, 20}, 'Insert as muted', mute_check);
    if was_checked ~= mute_check then -- persist setting on changed
        local v = '0';
        if mute_check then
            v = '1';
        end
        setprop('chk_muted', v);
    end

    was_checked = fulltake_check;
    fulltake_check = checkbox({30, 100, 200, 20}, 'Fill full take', fulltake_check);
    if was_checked ~= fulltake_check then -- persist setting on changed
        local v = '0';
        if fulltake_check then
            v = '1';
        end
        setprop('chk_fulltake', v);
    end

    label({10, 150, 200, 20}, 'History:');
    if button({270,150,100,20}, 'Clear') then
        chord_history = {};
        persist_history();
    end

    for idx = 1,8 do
        if chord_history[idx] == nil then
            break;
        end

        local row = (idx - 1) // 2;
        if (idx % 2) == 1 then
            if button({30, 180 + (row * 30), 150, 20}, chord_history[idx]) then
                do_chord_notation(chord_history[idx]);
            end
        else
            if button({220, 180 + (row * 30), 150, 20}, chord_history[idx]) then
                do_chord_notation(chord_history[idx]);
            end
        end
    end
end

-- The main loop of the plugin window.
-- It detects both mouse and keyboard input and calls 'do_gui' for
-- either repainting or event-dispatching.
function main_loop()
    if nil == reaper.MIDIEditor_GetActive() then
        return;
    end

    -- handle key input
    local ch = gfx.getchar()
    if (ch == 27) or (ch == -1) then
        return;
    else
        if 0 ~= ch then
            log(tostring(ch) .. '\n');

            gui_state.event_type = 3;
            gui_state.key_code = ch;
            do_gui();
        end
        reaper.defer(main_loop);
    end

    -- handle mouse input
    if (gfx.mouse_cap == 0) and (gui_state.mouse_down == true) then
        gui_state.mouse_down = false;
        log('MouseUp x:' .. gfx.mouse_x .. ', y:' .. gfx.mouse_y .. '\n');

        gui_state.event_type = 2;
        do_gui();
    elseif (gfx.mouse_cap & 1) ~= 0 then
        gui_state.mouse_down = true;

        gui_state.event_type = 1;
        do_gui();
    end

    gui_state.event_type = 0;
    do_gui();
    gfx.update();
end

-- load from persistent props
function load_settings()
    chordinput_state = {};

    mute_check = getprop('chk_muted');
    if mute_check == '1' then
        mute_check = true;
    else
        mute_check = false;
    end
    
    fulltake_check = getprop('chk_fulltake');
    if fulltake_check == '0' then
        fulltake_check = false;
    else
        fulltake_check = true;
    end
    
    chord_history = {};
    for i = 1,8 do
        local v = getprop('history' .. tostring(i));
        if v == nil then
            break;
        end
        chord_history[#chord_history + 1] = v;
    end
end

--[[ The entry point of plugin ]]

-- require an active MIDI editor to work
active_editor = reaper.MIDIEditor_GetActive();
if nil == active_editor then
    reaper.ShowMessageBox('No active MIDI editor.', 'Error', 0);
else
    gfx.init('Chord Donkey v0.1', 400, 300, 0, 100, 100)
    load_settings();
    reaper.defer(main_loop)
end
