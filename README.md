# Chord Donkey (Reaper Plugin)

**Chord Donkey** is a Reaper plugin writtern in Lua.

I develop it to help me composing game BGMs.

Just enter the chord notation string and it helps to insert the member notes of the chord in the MIDI editor.

Supprted chord notations (Since music is not my background, sorry if these chord notations seem wierd to you):
* Major chords: 'C', 'Dmaj7', 'Emaj9', etc ...
* Minor chords: 'Cm', 'Dm7', 'Em9', etc ...
* Dominant chords: 'C7', 'E9', etc ...
* Power chords: 'Epow', etc ...
* (Half) Diminished chords: 'Cdim7', 'Ehdim7', etc ...
* Support **add**, **sus**, **aug**, and **omit**: 'Dmaj7sus', 'Emaj9sus4', 'Cmadd2', 'E9omit3', etc ...
* Inversion: 'Dmaj7/F#', etc ...
* Member notes fine-tuning: 'Dmaj7(#3,b5)', etc ...
* Scale starts from a given tonic: 'Cion' (Ionian), 'Ddor' (Dorian), etc...

# Install
1. Have a working Reaper DAW
2. Download the 'chorddonkey.lua' and store it somewhere in your computer.
3. Launch Reaper.
4. Press '?' in Reaper to bring on action list dialog.
5. Click the 'Load...' button at the bottom-right corner of the dialog.
6. Pick the 'chorddonkey.lua' you just downloaded.
7. A new entry named: 'Script: chorddonkey.lua' should appear in the action list for now on. Select it and click the 'Run' button to run it. Assign a key binding on it is recommended.

# Usage
* Enter chord notation: Input the chord notation string in the text field. And click the 'Insert' button. The cooresponding member notes of the chord should be inserted to the current active MIDI editor. These notes are alway placed in the range of C4 ~ C5. You can later select them and move up/down to suit your need.
* Checkbox 'Insert as muted': Whether to insert the member notes as muted. It is not checked by default.
* Checkbox 'Fill full take': If checked, the member notes are inserted with length as long as the current editing MIDI take. If not checked, they will just be a grid long.
* History: Recently used chord notations (8 at most)
* Tips: In Reaper's MIDI editor, you can show notes from other track as faint for a quick referece. You do this by open the track list window within MIDI editor, and click on the eye icon of the reference track. Please note that selected or muted notes will not appear as faint.